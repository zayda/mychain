pragma solidity ^0.4.11;

contract Lottery {

    mapping(address => uint) usersBet;
    mapping(uint => address) users;
    uint userCount = 0;
    uint totalBets = 0;
    address owner;

    constructor() public {
        owner = msg.sender;
    }

    function bet() public payable  {
        if (msg.value > 0) {
            if (usersBet[msg.sender] == 0) {
                users[userCount] = msg.sender;
                userCount += 1;
            }
            usersBet[msg.sender] += msg.value;
            totalBets += msg.value;
        }
    }

    function end() public {
        assert(msg.sender == owner);

        uint sum = 0;
        uint winningNumber = uint(blockhash(block.number - 1)) % totalBets + 1;
        for (uint i = 0; i < userCount; i++) {
            sum += usersBet[users[i]];
            if (sum >= winningNumber) {
                selfdestruct(users[i]); // will send contract account balance to selected user address
                return;
            }
        }
    }

}