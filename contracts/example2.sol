/**
 * Example 2: Naming and voting
 */
pragma solidity ^0.4.0;

contract Naming {

    mapping (address => string) names;
    mapping (string => address) addresses;

    function set(address addr, string name) public {
        names[addr] = name;
        addresses[name] = addr;
    }

    function nameOf(address addr) view public returns (string) {
        return names[addr];
    }

    function addressOf(string name) view public returns (address) {
        return addresses[name];
    }

}

contract Election {

    mapping (address => address) public voters;
    mapping (address => uint8) votes;
    address[] public candidateList;

    constructor(address[] candidates) public {
        candidateList = candidates;
    }

    function totalVotesFor(address candidate) view public returns (uint8) {
        if (isValidCandidate(candidate)) {
            return votes[candidate];
        } else {
            return 0;
        }
    }

    function voteFor(address candidate) public {
        require(isValidCandidate(candidate), "Not a valid candidate");
        require(isValidVoter(msg.sender), "Not a valid voter");
        require(voters[msg.sender] == address(0), "Voter has already voted");

        voters[msg.sender] = candidate;
        votes[candidate] += 1;
    }

    function isValidVoter(address voter) pure public returns (bool) {
        return (voter != address(0)); //TODO e.g. if he is registered for election
    }

    function isValidCandidate(address candidate) view public returns (bool) {
        for(uint i = 0; i < candidateList.length; i++) {
            if (candidateList[i] == candidate) {
                return true;
            }
        }
        return false;
    }
}