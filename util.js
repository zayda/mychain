function checkAllBalances() {
    web3.eth.getAccounts(function(err, accounts) {
        accounts.forEach(function(id) {
            web3.eth.getBalance(id, function(err, balance) {
                console.log("" + id + ":\tbalance: " + web3.fromWei(balance, "ether") + " ether");
            });
        });
    });
};

function mineContract(_code, _account, _gas, _args) {

    var factory = eth.contract(JSON.parse(_code.abi));

    var instance = factory.new(_args, {from: _account, data: "0x" + _code.bin, gas: _gas}, function(e, contract) {
        if(e) {
            console.error(e); // If something goes wrong, at least we'll know.
            return;
        }

        if(!contract.address) {
            console.log("Contract transaction send, hash: " + contract.transactionHash + " waiting to be mined...");

        } else {
            console.log("Contract mined! Address: " + contract.address);
        }
    });
    return instance;
}





